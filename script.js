//'Экранирование необходимо для определения и поиска специальных символов в коде JS.

const newUser = createNewUser(); 

function createNewUser () {
     firstName = prompt('What is your name?');
     lastName = prompt('What is your surname?');
     birthday = prompt('When is your birthday? dd.mm.yyyy');
     birthDate = Date.parse(birthday.slice(6) + '.' + birthday.slice(3, 6) + birthday.slice(0, 2));
     today = new Date();

    const user = {
        firstName,
        lastName,
        birthDate,
        today,
        getAge() {return Math.floor((this.today - this.birthDate) / (1000 * 3600 * 24 * 365.2425))},
        getLogin() {return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()},
        getPassword() {return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + new Date(this.birthDate).getFullYear()},
    };    
    return user;
};

console.log(newUser.getAge());
console.log(newUser.getLogin());
console.log(newUser.getPassword());